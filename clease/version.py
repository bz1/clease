# pylint: disable=invalid-name
version_info = (0, 10, 2)

__version__ = '.'.join(map(str, version_info))

__all__ = ('__version__',)
