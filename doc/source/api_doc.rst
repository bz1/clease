API Documentation
=========================

.. toctree::
    ./api_settings
    ./api_newstruct
    ./api_evaluate
    ./api_fitting
    ./api_montecarlo
    ./api_mc_observers
    ./api_mc_constraints
    ./api_data_getters